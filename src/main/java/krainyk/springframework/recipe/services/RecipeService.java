package krainyk.springframework.recipe.services;

import krainyk.springframework.recipe.commands.RecipeCommand;
import krainyk.springframework.recipe.model.Recipe;

import java.util.Set;

public interface RecipeService {

    Set<Recipe> getRecipes();

    Recipe findById(Long id);

    RecipeCommand saveRecipeCommand(RecipeCommand command);

    RecipeCommand findCommandById(Long Id);

    void deleteById(Long id);
}
